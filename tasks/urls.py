from django.urls import path
from .views import show_assigned_tasks, create_task, show_task_detail, delete_task, complete_task

urlpatterns = [
    path("mine/", show_assigned_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
    path("<int:id>/", show_task_detail, name="show_task_detail"),
    path("<int:id>/delete/", delete_task, name="delete_task"),
    path("<int:id>/complete/", complete_task, name="complete_task")
]
