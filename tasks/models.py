from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField(auto_now_add=False)
    due_date = models.DateField(auto_now_add=False)
    is_completed = models.BooleanField(null=True, blank=False)
    project = models.ForeignKey(
        "projects.Project",  # possibly this only needs "Project"
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User, null=True, related_name="tasks", on_delete=models.CASCADE
    )
    description = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name
