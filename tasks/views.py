from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponse
from .models import Task
from django.contrib.auth.decorators import login_required
from .forms import CreateTaskForm


@login_required
def show_assigned_tasks(request):
    list_tasks = Task.objects.filter(assignee=request.user)
    context = {"list_tasks": list_tasks}
    return render(request, "tasks/mine.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        print(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.is_completed = False
            task.save()
            return redirect("show_project", id=task.project.id)
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_task_detail(request, id):
    task_detail = Task.objects.get(id=id)
    context = {"task_detail": task_detail}
    return render(request, "tasks/task_detail.html", context)


@login_required
def complete_task(request, id):
    task = Task.objects.get(id=id)
    task.is_completed = True
    task.save()
    return redirect('show_my_tasks')


@login_required
def delete_task(request, id):
    if request.method == "POST":
        task = get_object_or_404(Task, id=id, assignee=request.user)
        if task.is_completed:
            task.delete()
            return redirect("show_my_tasks")
        else:
            return HttpResponseBadRequest('Task must be completed before it can be deleted.')
    else:
        return HttpResponseBadRequest('Invalid request method.')
