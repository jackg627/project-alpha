from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProjectForm


@login_required
def show_all_projects(request):
    list_projects = Project.objects.all()
    context = {"list_projects": list_projects}
    return render(request, "projects/project_list.html", context)

@login_required
def show_user_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/user_project_list.html", context)


@login_required
def show_project_details(request, id):
    project_details = Project.objects.get(id=id)
    context = {"project_details": project_details}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm
    context = {"form": form}
    return render(request, "projects/create.html", context)



## ADD FUNCTIONALITY TO MARK PROJECT AS COMPLETED
