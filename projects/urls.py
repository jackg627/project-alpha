from django.urls import path
from .views import (
    show_all_projects,
    show_user_projects,
    show_project_details,
    create_project,
)

urlpatterns = [
    path("", show_all_projects, name="home"),
    path("project_list", show_all_projects, name="list_projects"),
    path("user_project_list", show_user_projects, name="user_projects"),
    path("<int:id>/", show_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
